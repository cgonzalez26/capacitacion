import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/models/clientes/cliente';
import { ClientesService } from 'src/app/services/clientes/clientes.service';

@Component({
  selector: 'app-clientes-create',
  templateUrl: './clientes-create.component.html',
  styleUrls: ['./clientes-create.component.css']
})
export class ClientesCreateComponent implements OnInit {
  cliente: Cliente = {
    Id: '0',
    Nombre: '',
    Apellido: '',
    Email: '',
    Telefono: '',
    Direccion: ''
  };

  constructor(
    private router: Router,
    private clientesService: ClientesService
  ) {     
  }

  ngOnInit(): void {
  }

  onSubmit(createForm: NgForm): void{
    this.cliente = this.saveCliente(createForm);
    console.log('datos cliente agregar',this.cliente);
   
    this.clientesService.add(this.cliente).subscribe(response => {
      console.log('respuesta add',response);      
      this.router.navigate(['/clientes'])
    }, error => console.log(error));    
    
    console.log(this.cliente);
  }

  saveCliente(createForm: NgForm) {
    const saveCliente = {
      Id : '',
      Nombre : createForm.value.Nombre,
      Apellido : createForm.value.Apellido,
      Email : createForm.value.Email,
      Telefono : createForm.value.Telefono,
      Direccion : createForm.value.Direccion
    };
    return saveCliente;
  }
}
