import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cliente } from 'src/app/models/clientes/cliente';
import { ClientesService } from 'src/app/services/clientes/clientes.service';

@Component({
  selector: 'app-clientes-list',
  templateUrl: './clientes-list.component.html',
  styleUrls: ['./clientes-list.component.css']
})
export class ClientesListComponent implements OnInit {
  clientes: Cliente[];

  constructor(
    private router: Router,
    private clientesService: ClientesService
  ) { 
    this.clientes = [];
  }

  ngOnInit(): void {  
    this.getAllClientes();      
  }   

  getAllClientes() {
    this.clientesService.getClientes().subscribe(response => {
      console.log('datos clientes ',response);
      this.clientes = response;
    }, error => console.log(error));
  }
  
  eliminarCliente(id : string) {
    const isOK = confirm('¿Etas seguro que desea eliminar el Cliente?');
    if (isOK) {
      this.clientesService.delete(id).subscribe(response => {
        console.log('respuesta delete ',response);        
        this.getAllClientes();
      }, error => console.log(error));
    }
  }

}
