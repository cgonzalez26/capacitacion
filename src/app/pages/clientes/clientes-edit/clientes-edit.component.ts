import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cliente } from 'src/app/models/clientes/cliente';
import { ClientesService } from 'src/app/services/clientes/clientes.service';

@Component({
  selector: 'app-clientes-edit',
  templateUrl: './clientes-edit.component.html',
  styleUrls: ['./clientes-edit.component.css']
})
export class ClientesEditComponent implements OnInit {
  id:string = '0';
  editForm: FormGroup  = new FormGroup({
    Nombre: new FormControl(),
    Apellido: new FormControl(),
    Email: new FormControl(),
    Telefono: new FormControl(),
    Direccion: new FormControl()
  });

  cliente: Cliente = {
    Id: '0',
    Nombre: '',
    Apellido: '',
    Email: '',
    Telefono: '',
    Direccion: ''
  };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _formBuilder: FormBuilder,
    private clientesService: ClientesService
  ) { 
    const params = this.activatedRoute.snapshot.params;
    if(params['id']) {
      this.id = params['id'];
    }
    this.getCliente(this.id);    
  }

  ngOnInit(): void {    
    
  }

  getCliente(id : string) {
    this.clientesService.getCliente(this.id).subscribe(response => {
      //console.log('cliente edit ',response);
      this.cliente = response;
      this.editForm = this.createDialogForm();
    }, error => console.log(error)); 
  }

  createDialogForm(): FormGroup {
    const formGroup = this._formBuilder.group({
      Nombre: [this.cliente.Nombre, Validators.required],
      Apellido: [this.cliente.Apellido],
      Email: [this.cliente.Email, [Validators.required, Validators.email] ],
      Telefono: [this.cliente.Telefono],
      Direccion: [this.cliente.Direccion]
    });
    console.log('edit form ',this.editForm);
    return formGroup;
  }

  onSubmit() {
    this.cliente = this.saveCliente();
    //console.log('datos cliente guardar',this.cliente);
    this.clientesService.update(this.cliente.Id, this.cliente).subscribe(response => {
      //console.log('respuesta edit',response);      
      this.router.navigate(['/clientes'])
    }, error => console.log(error));
  }

  saveCliente() {
    const saveCliente = {
      Id: this.id,
      Nombre: this.editForm.get('Nombre')?.value,
      Apellido: this.editForm.get('Apellido')?.value,
      Email: this.editForm.get('Email')?.value,
      Telefono: this.editForm.get('Telefono')?.value,
      Direccion: this.editForm.get('Direccion')?.value
    };
    return saveCliente;
  }

}
