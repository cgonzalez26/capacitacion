import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { ClientesCreateComponent } from './pages/clientes/clientes-create/clientes-create.component';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { ClientesEditComponent } from './pages/clientes/clientes-edit/clientes-edit.component';

const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'clientes/create', component: ClientesCreateComponent },
  { path: 'clientes/edit/:id', component: ClientesEditComponent },
  { path: 'clientes', component: ClientesComponent },
  { path: '**', component: InicioComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
