export class Cliente {
  public Id: string;
  public Nombre: string;
  public Apellido: string;
  public Email: string;
  public Telefono: string;
  public Direccion: string;
  constructor(id: string, nombre: string, apellido: string, email: string, telefono: string, direccion: string) {
    this.Id = id;
    this.Nombre = nombre;
    this.Apellido = apellido;
    this.Email = email;
    this.Telefono = telefono;
    this.Direccion = direccion;
  }
}
