import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { InicioComponent } from './pages/inicio/inicio.component';
import { AppRoutingModule } from './app-routing.module';
import { ClientesComponent } from './pages/clientes/clientes.component';
import { ClientesListComponent } from './pages/clientes/clientes-list/clientes-list.component';
import { ClientesEditComponent } from './pages/clientes/clientes-edit/clientes-edit.component';
import { ClientesCreateComponent } from './pages/clientes/clientes-create/clientes-create.component';
import { ClientesService } from './services/clientes/clientes.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    InicioComponent,
    ClientesComponent,
    ClientesListComponent,
    ClientesEditComponent,
    ClientesCreateComponent
  ],
  imports: [
    BrowserModule,    
    HttpClientModule,    
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [ClientesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
