import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cliente } from 'src/app/models/clientes/cliente';
import { environment } from 'src/environments/environment';

const API_URL = `${environment.apiUrl}/api/Clientes`;

@Injectable({
  providedIn: 'root',
})
export class ClientesService {
  clientes: Cliente[] = [
    {
      Id: '1',
      Nombre: 'Rodolfo',
      Apellido: 'Lopez',
      Email: 'rlopez@gmail.com',
      Telefono: '911111111',
      Direccion: 'Paseo de la Castellana, 100',
    },
    {
      Id:'2',
      Nombre: 'Marcos',
      Apellido: 'Alcaraz',
      Email: 'malcaraz@gmail.com',
      Telefono: '922222222',
      Direccion: 'Los Rosales, 200',
    },
  ];

  constructor(private http: HttpClient) {}

  /*getClientes() {
    return this.clientes;
  }

  getCliente(id: number): Cliente | undefined {
    return this.clientes.find((c) => c.id == id);
  }*/

  getClientes(): Observable<Cliente[]> {
    const url = API_URL + '/all';
    return this.http.get<Cliente[]>(url);
  }

  getCliente(id: string): Observable<Cliente> {
    const url = API_URL + '/id/' + id;
    return this.http.get<Cliente>(url);
  }

  add(cliente: Cliente) : Observable<Cliente>{
    const url = `${API_URL}/add`;
    return this.http.post<Cliente>(url, cliente);
  }
  
  update(id: string, cliente: Cliente): Observable<Cliente> {
    const url = `${API_URL}/edit/${id}`;
    return this.http.put<Cliente>(url, cliente);
  }
  
  delete(id: string): Observable<any> {
    const url = `${API_URL}/delete/${id}`;
    return this.http.delete(url);
  }
}
